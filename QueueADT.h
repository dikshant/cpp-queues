#ifndef QUEUEADT_H_
#define QUEUEADT_H_

template<class T> class QueueADT {

    //an ADT for a simple queue
public:
    virtual ~QueueADT() { }
    virtual bool Empty() = 0;
    virtual void Enqueue(const T& item) = 0;
    virtual T Dequeue() = 0;
};

#endif