#ifndef QUEUE_HPP_
#define QUEUE_HPP_

#include "QueueADT.h"
#include "Node.cpp"
#include "Underflow.h"

//Used 0 instead of NULL. Functionality should be the same.

template<class T>
class Queue : public QueueADT<T> {

private:

    //Pointers to keep track of what is at the back and the front of the queue
    Node<T> *frontPointer;
    Node<T> *backPointer;

public:

    //Initially both pointers are empty
    Queue() {
        frontPointer = 0;
        backPointer = 0;
    }

    //A destructor for the front and back pointers
    ~Queue() {
        delete frontPointer;
        delete backPointer;
    };

    // checks if the queue is empty by checking weather front pointer is empty or not.
    bool Empty() {
        return frontPointer == 0;

    }

    // adds a new item into the queue and adjusts the back pointer to denote the last thing that was enqueued
    // if there is only 1 item in the queue then both the front and the back pointer point to the same item
    void Enqueue(const T &item) {

        //add a new node to the queue
        Node<T> *dataNodePointer = new Node<T>(item);

        if (frontPointer == 0 && backPointer == 0) {
            backPointer = frontPointer = dataNodePointer;
        }

        //sets the back pointer to the next item that gets inserted.
        backPointer->next;
        backPointer = dataNodePointer;
    }


    //Returns the first item that was queued and adjusts the pointers.
    T Dequeue() {

        // if the pointers are empty then the queue is empty so it throws underflow
        if (frontPointer == 0 && backPointer == 0) {
            throw Underflow();
        }

        //Get the first item from the frontPointer which was the first item inserted into the queue
        T firstItem = frontPointer->listData;

        Node<T> *tmpNode = frontPointer;

        //Set the front pointer to the next item and delete the temporary node created to store the frontpointer
        frontPointer = frontPointer->next;
        delete tmpNode;

        //returns the first item that was inserted into the queue
        return firstItem;
    }

};

#endif