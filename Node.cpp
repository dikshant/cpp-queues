//Dikshant Adhikari
//Lab Section 22

#ifndef NODE_CPP_
#define NODE_CPP_

template<class T> class Node {


public:

    //item to be inserted into the node
    T listData;

    //next node to be pointed to
    Node *next;

    Node(const T item, Node<T> *nextNode = 0){
        listData = item;
        next = nextNode;
    }
};

#endif